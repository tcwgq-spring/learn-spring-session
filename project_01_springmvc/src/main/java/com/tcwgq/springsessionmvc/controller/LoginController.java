package com.tcwgq.springsessionmvc.controller;

import com.tcwgq.springsessionmvc.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * @author tcwgq
 * @since 2022/6/27 17:22
 **/
@Controller
@RequestMapping("/login")
public class LoginController {
    @GetMapping(value = "/to")
    public String to() {
        return "login";
    }

    @RequestMapping(value = "/do", method = {RequestMethod.GET, RequestMethod.POST})
    public String login(String username, String password, HttpSession session) {
        session.setAttribute(username, new User(username, password));
        return "login_success";
    }

}
