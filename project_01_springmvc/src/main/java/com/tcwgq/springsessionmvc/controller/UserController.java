package com.tcwgq.springsessionmvc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    /***
     * 用户信息
     */
    @GetMapping(value = "/info")
    public Object info(String username, HttpSession session) {
        return session.getAttribute(username);
    }

}
